package app;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private int threshold = 1000;
    private int surcharge = 10;

    public static void main(String[] args) {

        Main app = new Main();
        Scanner scanner = new Scanner(System.in);

        System.out.println("[?] Enter an amount (e.g. 10, 1000): ");
        String amount = scanner.next();

        if(app.approval(amount)) {
            System.out.println("[i] The amount requires approval");
        } else {
            System.out.println("[i] The amount does not require approval");
        }

    }

    public int parseInput(String value) {
        if (value == null) {
            throw new NullPointerException();
        } else {
            long parsed = Long.parseLong(value);
            if (parsed <= Integer.MIN_VALUE) {
                throw new ArithmeticException();
            } else if (parsed > Integer.MAX_VALUE) {
                throw new ArithmeticException();
            } else if (parsed <= 0) {
                throw new IllegalArgumentException();
            } else {
                return (int) parsed;
            }
        }
    }

    /**
     * Checks if a given ammount requires approval
     * Return true if a given amount is bigger than a given threshold
     * @return boolean
     */
    public boolean approval(String value){
        int parsed = parseInput(value);
        int amount = Math.addExact(parsed, surcharge);
        if (amount <= 0) {
            throw new IllegalArgumentException();
        } else if (amount >= threshold) {
            return true;
        }
        return false;
   }

}
